# Writing applications in Angular2 [part17] #

This repo contains just [part 14](https://bitbucket.org/tomask79/angular2-avoid-twd), but upgraded to the latest angular2 version, [RC5 build](https://github.com/angular/angular/blob/master/CHANGELOG.md).

## How to build Spring-Angular2 application with Maven  ##

As far as building of Angular2 application concerns I have taken a lot from this excellent article: 

```
http://blog.mgechev.com/2016/06/26/tree-shaking-angular2-production-build-rollup-javascript/
```

from **Minko Gechev** which I highly recommend to read. I have just upgraded package.json to be using RC5 libraries. I won't be retyping things from this article, just read it. Anyway, if you're full-stack developer then building with just NPM isn't enough for you. You typically want to run NPM from Maven build. You want your typescript files to be transpiled and packaged into one bundle file during build of your whole war file with backend files. Hence I have prepared Maven pom.xml file which runs package.json scripts commands prepared by Minko Gechev in the initialize, clean and compile maven build phase. Here it is, everything with help of maven_exec plugin:


```
.
.
	  <plugin>
			<groupId>org.codehaus.mojo</groupId>
			<artifactId>exec-maven-plugin</artifactId>
			<version>1.3.2</version>
			<executions>

				<execution>
					<id>npm i</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>initialize</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>i</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
				
				
				<execution>
					<id>npm_clean</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>clean</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>run</argument>
							<argument>clean</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
				
					
				<execution>
					<id>npm run build_prod</id>
					<goals>
						<goal>exec</goal>
					</goals>
					<phase>compile</phase>
					<configuration>
						<executable>npm</executable>
						<arguments>
							<argument>run</argument>
							<argument>build_prod</argument>
						</arguments>
						<workingDirectory>${basedir}/src/main/webapp</workingDirectory>
					</configuration>
				</execution>
			</executions>
		</plugin>	 
.
.
```
With build prepared like that resources will be packed into bundle.js and bundle.min.js files. Hence index.html will look like:

```
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title></title>
</head>
<body>
  <my-app>Loading...</my-app>
  <script src="/node_modules/zone.js/dist/zone.js"></script>
  <script src="/node_modules/reflect-metadata/Reflect.js"></script>
  <script src="dist/bundle.js"></script>
</body>
</html>
```

Btw you typically want your application to be running with SpringMVC backend, so don't forget to configure resources with redirection to index.html when hitting the '/' URL:

```
@Configuration
@EnableWebMvc
@ComponentScan(value = "org.mymvc.test")
public class ContextRootConfig extends WebMvcConfigurerAdapter {
	 @Override
	 public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	     registry.addResourceHandler("/**").addResourceLocations("/");
	     registry.addResourceHandler("/app/**").addResourceLocations("/app/");        
	     registry.addResourceHandler("/dist/**").addResourceLocations("/dist/");   
	     registry.addResourceHandler("/node_modules/**").addResourceLocations("/node_modules/");       
	     registry.addResourceHandler("/typings/**").addResourceLocations("/typings/");        
	 }

	 @Override
	 public void addViewControllers(final ViewControllerRegistry registry) {
	     registry.addViewController("/").setViewName("forward:/index.html");
	 }
}
```

## Test ##

* mvn clean install (in the root folder with pom.xml)
* mvn tomcat7:run-war-only
* in the browser hit http://localhost:8080 (part 14 should appear)

regards

Tomas