"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Person_1 = require('./Person');
var common_1 = require('@angular/common');
var PersonEditorComponent = (function () {
    function PersonEditorComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Person_1.Person)
    ], PersonEditorComponent.prototype, "person", void 0);
    PersonEditorComponent = __decorate([
        core_1.Component({
            selector: 'person-editor',
            template: "Name: <input type=text [(ngModel)]=\"person.name\" name=\"name\" />\n               Surname: <input type=text [(ngModel)]=\"person.surname\" name=\"surname\" />\n              ",
            directives: [common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [])
    ], PersonEditorComponent);
    return PersonEditorComponent;
}());
exports.PersonEditorComponent = PersonEditorComponent;
//# sourceMappingURL=person-editor.component.js.map