import {Component, Input} from '@angular/core';
import { Person } from './Person';
import { FORM_DIRECTIVES } from '@angular/common';

@Component({
    selector: 'person-editor',
    template: `Name: <input type=text [(ngModel)]="person.name" name="name" />
               Surname: <input type=text [(ngModel)]="person.surname" name="surname" />
              `,
    directives: [FORM_DIRECTIVES]
})

export class PersonEditorComponent {
    @Input() person: Person;
}