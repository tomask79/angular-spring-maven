package org.mymvc.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(value = "org.mymvc.test")
public class ContextRootConfig extends WebMvcConfigurerAdapter {
	 @Override
	 public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	     registry.addResourceHandler("/**").addResourceLocations("/");
	     registry.addResourceHandler("/app/**").addResourceLocations("/app/");        
	     registry.addResourceHandler("/dist/**").addResourceLocations("/dist/");   
	     registry.addResourceHandler("/node_modules/**").addResourceLocations("/node_modules/");       
	     registry.addResourceHandler("/typings/**").addResourceLocations("/typings/");        
	 }

	 @Override
	 public void addViewControllers(final ViewControllerRegistry registry) {
	     registry.addViewController("/").setViewName("forward:/index.html");
	 }
}
